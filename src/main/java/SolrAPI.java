//Use Apache Tika and Solr to crawl, index and search documents
//John Miedema http://johnmiedema.com
//-----------------------------------------------------------
//referenced libraries:
//Apache Tika 1.5
//Apache Solr 4.7.2
//Apache HttpClient 4.3.3 reqd to connect to Solr server
//Noggit json parser reqd for Solr commands
//-----------------------------------------------------------
//after Solr is downloaded, start it using the following commands
//cd path\solr-4.7.2\example
//java -jar start.jar
//-----------------------------------------------------------

import java.io.*;


import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

import org.apache.lucene.document.Document;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.*;
import org.apache.solr.common.params.MapSolrParams;
import org.apache.solr.schema.*;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.DublinCore;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.ocr.TesseractOCRConfig;
import org.apache.tika.parser.pdf.PDFParserConfig;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

public class SolrAPI {


    private final IndexSchema schema;
    private Document doc;
    private HashMap<String, String> map;


    public SolrAPI(IndexSchema schema) {
        this.schema = schema;
    }

    public void startDoc() {
        doc = new Document();
        map = new HashMap<String, String>();
    }



        private static SolrClient solr;

        public static void main(String[] args) throws IOException, SAXException, TikaException {

            final String solrUrl = "http://localhost:8983/solr";
            solr = new HttpSolrClient.Builder(solrUrl).withConnectionTimeout(10000)
                    .withSocketTimeout(60000)
                    .build();

            try {


//
//            String url = "http://localhost:8983/solr/customer";
//         //   solr = new HttpSolrServer("http://localhost:8983/solr/customer"); //create solr connection
//             solr = new CloudSolrClient.Builder().withSolrUrl(url).build();

                ///delete Query
                 final UpdateResponse response1 = solr.deleteByQuery("customer", "*:*"); //delete everything in the index; good for testing
                System.out.println(response1.getResponseHeader());
                System.out.println(response1.getStatus());

//            //location of source documents
//            //later this will be switched to a database
                String path = "C:\\Users\\k.bhakre\\Documents\\solr-7.2.1\\pdf\\";
                String file_html = path + "00 Agreement 2013 TRANSMISSION AGREEMENT Final Issue 28-1-2013.pdf";
                String file_txt = path + "IN006 O2 Equinix_MSAGLO-UK and GTC_310311.pdf";
                String file_pdf = path + "SL1045 Lease 2009.pdf";


//            solr.commit(); //after all docs are added, commit to the index

                //now you can search at http://localhost:8983/solr/browse

                final Map<String, String> queryParamMap = new HashMap<String, String>();
                queryParamMap.put("q", "*:*");
                queryParamMap.put("fl", "id, document,contents,url");
                MapSolrParams queryParams = new MapSolrParams(queryParamMap);
                processDocument(file_html);
                processDocument(file_txt);
                processDocument(file_pdf);
                solr.commit("customer");
                final QueryResponse response = solr.query("customer", queryParams);
                final SolrDocumentList documents = response.getResults();
                System.out.println(documents.toString());
                for (int i = 0; i < documents.size() ; i++) {

                    System.out.println(documents.get(i).getFieldNames());

                    System.out.println(documents.get(i).getFieldValue("document").getClass());
//                    System.out.println(documents.get(i).getFieldValue("url"));
//
//
//                   ArrayList abc=  (ArrayList<File>) documents.get(i).getFieldValue("contents");
//
//                    System.out.println(abc.get(0).toString());
//                    byte[]  nnn= (byte[] )abc.get(0);
//
//                //   System.out.println(new String(nnn));
//                    Path paths = Paths.get("test");
//                    Files.write(paths, nnn);
                }

//            processDocument(file_txt);
//            processDocument(file_pdf);


            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                System.out.println(ex.getStackTrace());
                System.out.println("main");
            }
        }

        private static void processDocument(String pathfilename) {

            try {
                File file = new File(pathfilename);
                InputStream input = new FileInputStream(file);




                //use Apache Tika to convert documents in different formats to plain text
                ContentHandler textHandler = new BodyContentHandler(10 * 1024 * 1024);
                Metadata metadata  = new     Metadata();
                Parser parser = new AutoDetectParser(); //handles documents in different formats:
                BodyContentHandler handler = new BodyContentHandler(Integer.MAX_VALUE);

                TesseractOCRConfig config = new TesseractOCRConfig();
                PDFParserConfig pdfConfig = new PDFParserConfig();
                pdfConfig.setExtractInlineImages(true);
                pdfConfig.setExtractUniqueInlineImagesOnly(false);


                ParseContext parseContext = new ParseContext();
                parseContext.set(TesseractOCRConfig.class, config);
                parseContext.set(PDFParserConfig.class, pdfConfig);
                //need to add this to make sure recursive parsing happens!
                parseContext.set(Parser.class, parser);
                ParseContext context = new ParseContext();

                parser.parse(input, handler, metadata , context); //convert to plain text

                //collect metadata and content from Tika and other sources

                //document id must be unique, use guid
                UUID guid = java.util.UUID.randomUUID();
                String docid = guid.toString();

                //Dublin Core metadata (partial set)
                String doctitle = metadata.get(DublinCore.TITLE);
                String doccreator = metadata.get(DublinCore.CREATOR);


                //other metadata
                String docurl = pathfilename; //document url
            //   System.out.println(metadata);
                //content
                String content = handler.toString();

                System.out.println("===============");
           //     System.out.println(content);
                System.out.println("Done");

           //     System.out.println(content);

                //call to index
                indexDocument(docid, doctitle, doccreator, docurl, content, file);


            //    PhoneExtractingContentHandler  phone = new PhoneExtractingContentHandler();


            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                System.out.println(ex.getStackTrace());
                System.out.println("Document");
            }
        }

    public static List<String> address_finder(String contains){

        Pattern p = Pattern.compile("(\\d+[ ](?:[A-Za-z0-9.-]+[,]?[ ]?)+)|((\\d+[a-z]?)[ ](?:[A-Za-z0-9.-]+[,]?[ ]?)+)");
        List<String> numbers = new ArrayList<String>();
        Matcher m = p.matcher(contains.replaceAll("\n"," "));
        while (m.find()) {
            numbers.add(m.group());
        }
        return numbers;
    }
    public static Set<String> findnumbers(String contains){

        Pattern p = Pattern.compile("[\\+\\(]?[0-9][0-9 \\-\\(\\)]{8,}[0-9]");
        List<String> numbers = new ArrayList<String>();
        Matcher m = p.matcher(contains);
        while (m.find()) {
            numbers.add(m.group());
        }
        return  new HashSet<String>(numbers);
    }

    public static List<String> findaddress(String contains){

        Pattern p = Pattern.compile("^[A-Z]{1,2}[0-9]{1,2} ?[0-9][A-Z]{2}(GIR 0AA)|((([ABCDEFGHIJKLMNOPRSTUWYZ][0-9][0-9]?)|(([ABCDEFGHIJKLMNOPRSTUWYZ][ABCDEFGHKLMNOPQRSTUVWXY][0-9][0-9]?)|(([ABCDEFGHIJKLMNOPRSTUWYZ][0-9][ABCDEFGHJKSTUW])|([ABCDEFGHIJKLMNOPRSTUWYZ][ABCDEFGHKLMNOPQRSTUVWXY][0-9][ABEHMNPRVWXY])))) [0-9][ABDEFGHJLNPQRSTUWXYZ]{2})");
        List<String> numbers = new ArrayList<String>();
        Matcher m = p.matcher(contains);
        while (m.find()) {
//            numbers.add(address_finder(m.group()));
//            System.out.println(m.group());
//            System.out.println(contains.substring(  StringUtils.indexOf(contains,m.group())-40, 8+ StringUtils.indexOf(contains,m.group())));
//            System.out.println(StringUtils.indexOf(contains,m.group())-40);
//            System.out.println(8+ StringUtils.indexOf(contains,m.group()));
//            System.out.println(address_finder(contains.substring(  StringUtils.indexOf(contains,m.group())-40, 8+ StringUtils.indexOf(contains,m.group()))));
//            System.out.println(address_finder(contains.substring(  StringUtils.indexOf(contains,m.group())-40, 8+ StringUtils.indexOf(contains,m.group()))));
            numbers.add(address_finder(contains.substring(  StringUtils.indexOf(contains,m.group())-40, 8+ StringUtils.indexOf(contains,m.group()))).get(0));
        }
        return numbers;
    }
        private static void indexDocument(String docid, String doctitle, String doccreator, String docurl, String doccontent, File file) {

            try {
                SolrInputDocument doc = new SolrInputDocument();

                doc.addField("id", docid);
                //map metadata fields to default schema
                //location: path\solr-4.7.2\example\solr\collection1\conf\schema.xml

                //Dublin Core
                //thought: schema could be modified to use Dublin Core
                   doc.addField("title", doctitle);
                doc.addField("author", doccreator);


                //other metadata
                doc.addField("url", (file));
//
             //   doc.addField("document", readFileToByteArray(file) );


                doc.setField( "Phone_numbers",findnumbers(doccontent));
                doc.setField("documents", doccontent);

                doc.setField("Address",(findaddress(doccontent)));



                //content (and text)
                //per schema, the content field is not indexed by default, used for returning and highlighting document content
                //the schema "copyField" command automatically copies this to the "text" field which is indexed

                //================================================= to Store File
                //doc.addField("contents",  getBytesFromFile(file));
                //=================================================


                //indexing
                //when a field is indexed, like "text", Solr will handle tokenization, stemming, removal of stopwords etc, per the schema defn

                //add to index
                //  solr.add(doc);

                final UpdateResponse updateResponse = solr.add("customer", doc);
//// Indexed documents must be committed

            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                System.out.println("Index");
            }
        }

    // Returns the contents of the file in a byte array.
    public static byte[] getBytesFromFile(File file) throws IOException {
        // Get the size of the file
        long length = file.length();

        // You cannot create an array using a long type.
        // It needs to be an int type.
        // Before converting to an int type, check
        // to ensure that file is not larger than Integer.MAX_VALUE.
        if (length > Integer.MAX_VALUE) {
            // File is too large
            throw new IOException("File is too large!");
        }

        // Create the byte array to hold the data
        byte[] bytes = new byte[(int)length];

        // Read in the bytes
        int offset = 0;
        int numRead = 0;

        InputStream is = new FileInputStream(file);
        try {
            while (offset < bytes.length
                    && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
                offset += numRead;
            }
        } finally {
            is.close();
        }

        // Ensure all the bytes have been read in
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "+file.getName());
        }
        return bytes;
    }
    }