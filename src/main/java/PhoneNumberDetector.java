import com.google.i18n.phonenumbers.Phonenumber;
import org.apache.commons.codec.language.bm.Rule;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.apache.tika.sax.PhoneExtractingContentHandler;
import org.xml.sax.SAXException;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneNumberDetector {

   public static List<String> getphoneNumbera(String filepath) throws TikaException, SAXException, IOException {

       Parser parser = new AutoDetectParser();
       Metadata metadata = new Metadata();
       PhoneExtractingContentHandler handler = new PhoneExtractingContentHandler(new  BodyContentHandler(), metadata);
       handler.characters(new char[]{'+'},0,10);
       InputStream stream = new BufferedInputStream( new FileInputStream(filepath));
       handler.startPrefixMapping("+","");
       parser.parse(stream, handler, metadata, new ParseContext());
      // System.out.println(Arrays.asList(metadata.names()));

       //System.out.println(handler.toString());

       Pattern p = Pattern.compile("[\\+\\(]?[1-9][0-9 \\-\\(\\)]{8,}[0-9]");
       List<String> numbers = new ArrayList<String>();
       Matcher m = p.matcher(handler.toString());
       while (m.find()) {
           numbers.add(m.group());
       }
       System.out.println(numbers);

       String[] numbers1 = metadata.getValues("phonenumbers");
       stream.close();
       return Arrays.asList(numbers1);
    }

    public static List<String> findnumbers(String contains){

        Pattern p = Pattern.compile("[\\+\\(]?[1-9][0-9 \\-\\(\\)]{8,}[0-9]");
        List<String> numbers = new ArrayList<String>();
        Matcher m = p.matcher(contains);
        while (m.find()) {
            numbers.add(m.group());
        }
        return numbers;
    }

}
